"""Highest drop in time period."""


import pandas as pd
import datetime as dt
import numpy as np

# Generating one year data
# starting tine
start = dt.datetime(2018, 1, 1)
# ending time
end = dt.datetime(2018, 12, 31, 23, 59)
# generating time stamp
time_stamp = pd.date_range(start=start, end=end, freq='30s')
size = len(time_stamp)
df = pd.DataFrame()
df['DATE'] = time_stamp.date
df['TIME'] = time_stamp.time
sub_mat_name = {1: 'CG_MAIDA_50kg', 2: 'G_MAIDA_50KG',
                3: 'BREAD_MAIDA_50KG', 4: 'CG_RAWA_50KG',
                5: 'BELL_RAWA_50KG', 6: 'BELL_CHIROTY_50KG',
                7: 'CG_ATTA_50KG', 8: 'BELL_ATTA_50KG',
                9: 'G_ATTA_50kG', 10: 'CGC_ATTA_25KG',
                11: 'CGC_ATTA_50KG', 12: 'BELL_BRAN_49KG',
                13: 'SD_45KG', 14: 'BELL_SF49KG',
                15: 'CG_FLAKES_34KG', 16: 'CG_FLAKES_30KG'}

lst = list(range(1, 17))
prob = [0, 0.5, 0, 0.07, 0, 0.051, 0.051, 0.02,
        0.015, 0, 0, 0.09, 0.015, 0.098, 0.075, 0.015]
df['SUB_MATERIAL'] = np.random.choice(lst, size, p=prob)
df['MONTH'] = time_stamp.month
df['SEASON'] = df['MONTH'] // 4.5 + 1


def min_avg_using_expanding(x):
    """Find min production of bw time period."""
    x_ind = x.index
    minavg = 1000000
    for i in range(len(x)):
        comm_mean = x.loc[x_ind[i:]].expanding().mean()
        this_min_avg = comm_mean.min()[0]
        if this_min_avg < minavg:
            stime = x_ind[i]
            etime = comm_mean.idxmin()[0]
            minavg = this_min_avg
    return minavg, stime, etime


# Highest drop in production on which day b/w which time period for category 2
# transforming dataframe
hour = df['TIME'].apply(lambda x: x.hour)
x = (df.groupby(['DATE', hour, 'SUB_MATERIAL'])['TIME']
       .count()
       .rename('BAGS_COUNTS')
       .to_frame()
       .reset_index())
x['DATE'] = x['DATE'].astype('datetime64[ns]')
x = x[x['SUB_MATERIAL'] == 2]
x = x[['DATE', 'TIME', 'BAGS_COUNTS']].set_index(['DATE', 'TIME'])
all_days_min_avg = pd.DataFrame()
ind_date = x.index.get_level_values(0).unique()
for i in ind_date:
    minavg, stime, etime = min_avg_using_expanding(x.loc[i])
    day_min_avg = (pd.Series([stime, etime, minavg],
                             index=['Stime', 'Etime', 'Minavg'],
                             name=str(i)[:10]))
    all_days_min_avg = pd.concat([all_days_min_avg, day_min_avg], axis=1)
all_days_min_avg = all_days_min_avg.T
all_days_min_avg.loc[all_days_min_avg['Minavg'].idxmin()]
