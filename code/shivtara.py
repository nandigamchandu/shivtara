"""Data generate for shivtara mill."""

# date / time interval / all 17 materials

import pandas as pd
import datetime as dt
import numpy as np
import seaborn as sns
# import matplotlib as plt

# starting time
start = dt.datetime(2018, 1, 1, 6)
# ending time
end = dt.datetime(2019, 1, 1, 6)
# time range label
labels = ['24-1', '1-2', '2-3', '3-4', '4-5', '5-6', '6-7', '7-8', '8-9',
          '9-10', '10-11', '11-12', '12-13', '13-14', '14-15', '15-16',
          '16-17', '17-18', '18-19', '19-20', '20-21', '21-22', '22-23',
          '23-24']
# order
labels1 = ['1-2', '2-3', '3-4', '4-5', '5-6', '6-7', '7-8', '8-9',
           '9-10', '10-11', '11-12', '12-13', '13-14', '14-15', '15-16',
           '16-17', '17-18', '18-19', '19-20', '20-21', '21-22', '22-23',
           '23-24', '24-1']

# month labels
months = ['Jan', 'Feb', 'Mar', 'Apr',
          'May', 'Jun', 'Jul', 'Aug',
          'Sep', 'Oct', 'Nov', 'Dec']
time_24 = list(range(0, 24))
labels = dict(zip(time_24, labels))

# generating date range with hour frequency
x = pd.date_range(start=start, end=end, freq='h')

# column names
columns = [('MAIDA', 'CG_MAIDA_50kg'),
           ('MAIDA', 'G_MAIDA_50KG'),
           ('MAIDA', 'BREAD_MAIDA_50KG'),
           ('RAWA', 'CG_RAWA_50KG'),
           ('RAWA', 'BELL_RAWA_50KG'),
           ('RAWA', 'BELL_CHIROTY_50KG'),
           ('ATTA', 'CG_ATTA_50KG'),
           ('ATTA', 'BELL_ATTA_50KG'),
           ('ATTA', 'G_ATTA_50kG'),
           ('ATTA', 'CGC_ATTA_25KG'),
           ('ATTA', 'CGC_ATTA_50KG'),
           ('BRAN', 'BELL_BRAN_49KG'),
           ('BRAN', 'SD_45KG'),
           ('BRAN', 'BELL_SF49KG'),
           ('BRAN', 'CG_FLAKES_34KG'),
           ('BRAN', 'CG_FLAKES_30KG')]

# multiple indexing
col = pd.MultiIndex.from_tuples(columns, names=['MAIN', 'SUB'])
df = pd.DataFrame(columns=col)

# assigning date
df['DATE'] = x.date
# assigning time
df['TIME'] = x.hour
# Replacing time
df['TIME_RANGE'] = df['TIME'].replace(labels, axis=0)
# setting TIME_RANGE column as order Categorical type
df['TIME_RANGE'] = pd.Categorical(df['TIME_RANGE'], categories=labels1,
                                  ordered=True)
size = len(df)
# Assigning each product type
df[('MAIDA', 'CG_MAIDA_50kg')] = np.nan
df[('MAIDA', 'G_MAIDA_50KG')] = np.random.randint(58, 69, size)
df[('MAIDA', 'BREAD_MAIDA_50KG')] = np.nan
df[('RAWA', 'CG_RAWA_50KG')] = np.random.randint(9, 12, size)
df[('RAWA', 'BELL_RAWA_50KG')] = np.nan
df[('RAWA', 'BELL_CHIROTY_50KG')] = np.random.randint(5, 8, size)
df[('ATTA', 'CG_ATTA_50KG')] = np.random.randint(8, 11, size)
df[('ATTA', 'BELL_ATTA_50KG')] = np.random.randint(9, 11, size)
df[('ATTA', 'G_ATTA_50kG')] = np.random.randint(1, 3, size)
df[('ATTA', 'CGC_ATTA_25KG')] = np.nan
df[('ATTA', 'CGC_ATTA_50KG')] = np.nan
df[('BRAN', 'BELL_BRAN_49KG')] = np.random.randint(11, 16, size)
df[('BRAN', 'SD_45KG')] = np.random.randint(1, 2, size)
df[('BRAN', 'BELL_SF49KG')] = np.random.randint(10, 16, size)
df[('BRAN', 'CG_FLAKES_34KG')] = np.random.randint(6, 16, size)
df[('BRAN', 'CG_FLAKES_30KG')] = np.nan
df = df.set_index(['DATE', 'TIME_RANGE'])
df = df.drop('TIME', axis=1)

# fill missing values with zeros
df = df.fillna(0)

# calculate GRINDING
df['GRINDING'] = (50 * df[('MAIDA', 'CG_MAIDA_50kg')] +
                  50 * df[('MAIDA', 'G_MAIDA_50KG')] +
                  50 * df[('MAIDA', 'BREAD_MAIDA_50KG')] +
                  50 * df[('RAWA', 'CG_RAWA_50KG')] +
                  50 * df[('RAWA', 'BELL_RAWA_50KG')] +
                  50 * df[('RAWA', 'BELL_CHIROTY_50KG')] +
                  50 * df[('ATTA', 'CG_ATTA_50KG')] +
                  50 * df[('ATTA', 'BELL_ATTA_50KG')] +
                  50 * df[('ATTA', 'G_ATTA_50kG')] +
                  25 * df[('ATTA', 'CGC_ATTA_25KG')] +
                  50 * df[('ATTA', 'CGC_ATTA_50KG')] +
                  49 * df[('BRAN', 'BELL_BRAN_49KG')] +
                  45 * df[('BRAN', 'SD_45KG')] +
                  49 * df[('BRAN', 'BELL_SF49KG')] +
                  34 * df[('BRAN', 'CG_FLAKES_34KG')] +
                  30 * df[('BRAN', 'CG_FLAKES_30KG')])

df.head(10)
df1 = df

# shiv tara data analysis
df1 = df1.reset_index()


# %%
def avg_amt_wheat_for_hour(df):
    """Average amount of wheat grinded for hour per day."""
    month = df['DATE'].apply(lambda x: x.strftime('%b'))
    time_range = df['TIME_RANGE']
    x1 = df1.groupby([month, time_range], sort=False)['GRINDING'].sum()
    x2 = df1.groupby([month, time_range], sort=False)['GRINDING'].count()
    x = pd.DataFrame(x1/x2).unstack(level=0)
    x.columns = x.columns.get_level_values(1)
    x = x.rename_axis('Month', axis=1)
    return x


x = avg_amt_wheat_for_hour(df1)
sns.heatmap(x, cmap='Blues')


# %%
def avg_amt_wheat_per_day(df):
    """Average amount of wheat grinded per day for month."""
    month = df['DATE'].apply(lambda x: x.strftime('%b'))
    x1 = df.groupby(month, sort=False)['GRINDING'].sum()
    x2 = df.groupby(month, sort=False)['GRINDING'].count()
    x = pd.DataFrame(x1/x2)
    return x


x = avg_amt_wheat_per_day(df1)
ax = sns.barplot(x.index, x['GRINDING'])
ax.set(xlabel='Month', ylabel='kgs of wheat grinded per day',
       title='kgs of wheat grinded per day for month')
sns.despine()
